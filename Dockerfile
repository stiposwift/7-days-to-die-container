FROM --platform=linux/amd64 golang:1.23 AS builder

WORKDIR /app
COPY cmd cmd
COPY pkg pkg
COPY go.mod go.mod

RUN go mod tidy && go build -o /bin/seven-server cmd/7dtd-server/main.go && chmod +x /bin/seven-server

FROM --platform=linux/amd64 debian:bookworm-slim

ARG TZ=America/New_York
ENV TZ=${TZ}

# Skip any interactive post-install steps
ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies
RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    ca-certificates \
    curl \
    libc6-i386 \
    libgcc1 \
    lib32stdc++6 \
    lib32gcc-s1 \
    tzdata && \
    useradd -m steam && \
    mkdir -p /home/steam/steamcmd && \
    chown -R steam:steam /home/steam/steamcmd

USER steam

# Install steam
WORKDIR /home/steam/steamcmd
RUN ls -lha && whoami &&  curl -o /home/steam/steamcmd/steamcmd_linux.tar.gz https://media.steampowered.com/client/steamcmd_linux.tar.gz && \
    tar -xf steamcmd_linux.tar.gz && \
    rm steamcmd_linux.tar.gz && \
    chmod +x ./steamcmd.sh && \
    ./steamcmd.sh +quit

WORKDIR /home/steam

COPY --from=builder /bin/seven-server /home/steam/seven-server

EXPOSE 26900
EXPOSE 8080
EXPOSE 8081
EXPOSE 26902/udp

ENTRYPOINT [ "/home/steam/seven-server" ]
