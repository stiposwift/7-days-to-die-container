package steam

import (
	"fmt"
	"log/slog"
	"os"
	"os/exec"
	"strings"
)

func InstallGame(gameId int, installDirectory, betaName string) error {
	gameArg := fmt.Sprintf("%d", gameId)
	if betaName != "" {
		gameArg = fmt.Sprintf("'%s -beta %s", gameArg, betaName)
	}

	cmds := []string{
		"+@ShutdownOnFailedCommand", "1",
		"+force_install_dir", installDirectory,
		"+login", "anonymous",
		"+app_update", gameArg,
		"+quit",
	}

	script := fmt.Sprintf("/home/steam/steamcmd/steamcmd.sh %s", strings.Join(cmds, " "))

	slog.Info("running script", slog.String("script", script))

	cmd := exec.Command(
		"/bin/bash", "-c",
		script,
	)

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
