package server

import (
	"fmt"
	"log/slog"
	"os"
	"os/exec"
	"strings"
	"sync"
)

func Run7Days(installDir, configFile, logFile string) error {

	cmds := []string{
		"7DaysToDieServer.x86_64",
		fmt.Sprintf("-configfile=%s", configFile),
		fmt.Sprintf("-logfile=%s", logFile),
		"-quit",
		"-batchmode",
		"-nographics",
		"-dedicated",
	}

	script := fmt.Sprintf("%s/%s", installDir, strings.Join(cmds, " "))

	slog.Info("running script", slog.String("script", script))

	cmd := exec.Command("/bin/bash", "-c", script)
	cmd.Dir = installDir
	cmd.Env = []string{"LD_LIBRARY_PATH=."}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		return err
	}
	// tail the log in a thread
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		cmd := exec.Command("tail", "-f", logFile)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err = cmd.Run(); err != nil {
			slog.Error("error tailing log", slog.Any("error", err))
		}
		wg.Done()
	}()
	wg.Wait()
	return nil
}
