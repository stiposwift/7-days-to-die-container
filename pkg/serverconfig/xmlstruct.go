package serverconfig

import "encoding/xml"

type ServerSettingsBlock struct {
	XMLName    xml.Name                 `xml:"ServerSettings"`
	Properties []ServerSettingsProperty `xml:"property"`
}

type ServerSettingsProperty struct {
	XMLName xml.Name `xml:"property"`
	Name    string   `xml:"name,attr"`
	Value   string   `xml:"value,attr"`
}

// GetValue will get the Property value attribute for a given name attribute
func (b *ServerSettingsBlock) GetValue(n string) (string, bool) {
	for _, p := range b.Properties {
		if p.Name == n {
			return p.Value, true
		}
	}
	return "", false
}
