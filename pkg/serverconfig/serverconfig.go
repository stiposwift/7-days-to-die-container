// This code is generated
// Do not edit it directly
// Rerun the generate-serverconfig main package
//
// GOFILE_LOCATION=pkg/serverconfig/serverconfig.go go run cmd/generate-serverconfig/main.go

package serverconfig

import (
	"encoding/xml"
	"errors"
	"log/slog"
	"os"
	"strconv"
)

type ServerSettings struct {
	 // Whatever you want the name of the server to be.
	ServerName string
	 // Whatever you want the server description to be, will be shown in the server browser.
	ServerDescription string
	 // Website URL for the server, will be shown in the serverbrowser as a clickable link.
	ServerWebsiteURL string
	 // Password to gain entry to the server.
	ServerPassword string
	 // If set the user will see the message during joining the server and has to confirm it before continuing.
	 // For more complex changes to this window you can change the "serverjoinrulesdialog" window in XUi.
	ServerLoginConfirmationText string
	 // The region this server is in.
	 // Values: NorthAmericaEast, NorthAmericaWest, CentralAmerica, SouthAmerica, Europe, Russia, Asia, MiddleEast, Africa, Oceania.
	Region string
	 // Primary language for players on this server.
	 // Values: Use any language name that you would users expect to search for.
	 // Should be the English name of the language, e.
	 // g.
	 // not "Deutsch" but "German".
	Language string
	 // Port you want the server to listen on.
	 // Keep it in the ranges 26900 to 26905 or 27015 to 27020 if you want PCs on the same LAN to find it as a LAN server.
	ServerPort int
	 // Visibility of this server: 2 = public, 1 = only shown to friends, 0 = not listed.
	 // As you are never friend of a dedicated server setting this to "1" will only work when the first player connects manually by IP.
	ServerVisibility int
	 // Networking protocols that should not be used.
	 // Separated by comma.
	 // Possible values: LiteNetLib, SteamNetworking.
	 // Dedicated servers should disable SteamNetworking if there is no NAT router in between your users and the server or when port-forwarding is set up correctly.
	ServerDisabledNetworkProtocols string
	 // Maximum (!) speed in kiB/s the world is transferred at to a client on first connect if it does not have the world yet.
	 // Maximum is about 1300 kiB/s, even if you set a higher value.
	ServerMaxWorldTransferSpeedKiBs int
	 // Maximum Concurrent Players.
	ServerMaxPlayerCount int
	 // Out of the MaxPlayerCount this many slots can only be used by players with a specific permission level.
	ServerReservedSlots int
	 // Required permission level to use reserved slots above.
	ServerReservedSlotsPermission int
	 // This many admins can still join even if the server has reached MaxPlayerCount.
	ServerAdminSlots int
	 // Required permission level to use the admin slots above.
	ServerAdminSlotsPermission int
	 // Enable/disable the web dashboard.
	WebDashboardEnabled bool
	 // Port of the web dashboard.
	WebDashboardPort int
	 // External URL to the web dashboard if not just using the public IP of the server, e.
	 // g.
	 // if the web dashboard is behind a reverse proxy.
	 // Needs to be the full URL, like "https://domainOfReverseProxy.
	 // tld:1234/".
	 // Can be left empty if directly using the public IP and dashboard port.
	WebDashboardUrl string
	 // Enable/disable rendering of the map to tile images while exploring it.
	 // This is used e.
	 // g.
	 // by the web dashboard to display a view of the map.
	EnableMapRendering bool
	 // Enable/Disable the telnet.
	TelnetEnabled bool
	 // Port of the telnet server.
	TelnetPort int
	 // Password to gain entry to telnet interface.
	 // If no password is set the server will only listen on the local loopback interface.
	TelnetPassword string
	 // After this many wrong passwords from a single remote client the client will be blocked from connecting to the Telnet interface.
	TelnetFailedLoginLimit int
	 // How long will the block persist (in seconds).
	TelnetFailedLoginsBlocktime int
	 // Show a terminal window for log output / command input (Windows only).
	TerminalWindowEnabled bool
	 // Server admin file name.
	 // Path relative to UserDataFolder/Saves.
	AdminFileName string
	 // Use this to override where the server stores all user data, including RWG generated worlds and saves.
	 // Do not forget to uncomment the entry!.
	UserDataFolder string
	 // Enables/Disables EasyAntiCheat.
	EACEnabled bool
	 // Ignore EOS sanctions when allowing players to join.
	IgnoreEOSSanctions bool
	 // Hide logging of command execution.
	 // 0 = show everything, 1 = hide only from Telnet/ControlPanel, 2 = also hide from remote game clients, 3 = hide everything.
	HideCommandExecutionLog int
	 // Override how many chunks can be uncovered on the ingame map by each player.
	 // Resulting max map file size limit per player is (x * 512 Bytes), uncovered area is (x * 256 m²).
	 // Default 131072 means max 32 km² can be uncovered at any time.
	MaxUncoveredMapChunksPerPlayer int
	 // If disabled a player can join with any selected profile.
	 // If true they will join with the last profile they joined with.
	PersistentPlayerProfiles bool
	 // The number of in-game days which must pass since visiting a chunk before it will reset to its original state if not revisited or protected (e.
	 // g.
	 // by a land claim or bedroll being in close proximity).
	MaxChunkAge int
	 // The maximum disk space allowance for each saved game in megabytes (MB).
	 // Saved chunks may be forceably reset to their original states to free up space when this limit is reached.
	 // Negative values disable the limit.
	SaveDataLimit int
	 // "RWG" (see WorldGenSeed and WorldGenSize options below) or any already existing world name in the Worlds folder (currently shipping with e.
	 // g.
	 // "Navezgane", "Pregen04k1", "Pregen04k2", "Pregen04k3", "Pregen04k4", "Pregen06k1", "Pregen06k2", "Pregen06k3", "Pregen06k4", "Pregen08k1", "Pregen08k2", "Pregen08k3", "Pregen08k4", "Pregen10k1", "Pregen10k2", "Pregen10k3", "Pregen10k4", …).
	GameWorld string
	 // If RWG this is the seed for the generation of the new world.
	 // If a world with the resulting name already exists it will simply load it.
	WorldGenSeed string
	 // If RWG, this controls the width and height of the created world.
	 // Officially supported sizes are between 6144 and 10240 and must be a multiple of 2048, e.
	 // g.
	 // 6144, 8192, 10240.
	WorldGenSize int
	 // Whatever you want the game name to be (allowed [A-Za-z0-9_-.
	 // ]).
	 // This affects the save game name as well as the seed used when placing decoration (trees etc) in the world.
	 // It does not control the generic layout of the world if creating an RWG world.
	GameName string
	 // GameModeSurvival.
	GameMode string
	 // 0 - 5, 0=easiest, 5=hardest.
	GameDifficulty int
	 // How much damage do players to blocks (percentage in whole numbers).
	BlockDamagePlayer int
	 // How much damage do AIs to blocks (percentage in whole numbers).
	BlockDamageAI int
	 // How much damage do AIs during blood moons to blocks (percentage in whole numbers).
	BlockDamageAIBM int
	 // XP gain multiplier (percentage in whole numbers).
	XPMultiplier int
	 // If a player is less or equal this level he will create a safe zone (no enemies) when spawned.
	PlayerSafeZoneLevel int
	 // Hours in world time this safe zone exists.
	PlayerSafeZoneHours int
	 // cheat mode on/off.
	BuildCreate bool
	 // real time minutes per in game day: 60 minutes.
	DayNightLength int
	 // in game hours the sun shines per day: 18 hours day light per in game day.
	DayLightLength int
	 // Penalty after dying.
	 // 0 = Nothing.
	 // 1 = Default: Classic XP Penalty.
	 // 2 = Injured: You keep most of your debuffs.
	 // Food and Water is set to 50% on respawn.
	 // 3 = Permanent Death: Your character is completely reset.
	 // You will respawn with a fresh start within the saved game.
	DeathPenalty int
	 // 0 = nothing, 1 = everything, 2 = toolbelt only, 3 = backpack only, 4 = delete all.
	DropOnDeath int
	 // 0 = nothing, 1 = everything, 2 = toolbelt only, 3 = backpack only.
	DropOnQuit int
	 // Size (box "radius", so a box with 2 times the given value for each side's length) of bedroll deadzone, no zombies will spawn inside this area, and any cleared sleeper volumes that touch a bedroll deadzone will not spawn after they've been cleared.
	BedrollDeadZoneSize int
	 // Number of real world days a bedroll stays active after owner was last online.
	BedrollExpiryTime int
	 // This setting covers the entire map.
	 // There can only be this many zombies on the entire map at one time.
	 // Changing this setting has a huge impact on performance.
	MaxSpawnedZombies int
	 // If your server has a large number of players you can increase this limit to add more wildlife.
	 // Animals don't consume as much CPU as zombies.
	 // NOTE: That this doesn't cause more animals to spawn arbitrarily: The biome spawning system only spawns a certain number of animals in a given area, but if you have lots of players that are all spread out then you may be hitting the limit and can increase it.
	MaxSpawnedAnimals int
	 // Max viewdistance a client may request (6 - 12).
	 // High impact on memory usage and performance.
	ServerMaxAllowedViewDistance int
	 // Maximum amount of Chunk mesh layers that can be enqueued during mesh generation.
	 // Reducing this will improve memory usage but may increase Chunk generation time.
	MaxQueuedMeshLayers int
	 // Enable/Disable enemy spawning.
	EnemySpawnMode bool
	 // 0 = Normal, 1 = Feral.
	EnemyDifficulty int
	 // 0-3 (Off, Day, Night, All).
	ZombieFeralSense int
	 // 0-4 (walk, jog, run, sprint, nightmare).
	ZombieMove int
	 // 0-4 (walk, jog, run, sprint, nightmare).
	ZombieMoveNight int
	 // 0-4 (walk, jog, run, sprint, nightmare).
	ZombieFeralMove int
	 // 0-4 (walk, jog, run, sprint, nightmare).
	ZombieBMMove int
	 // What frequency (in days) should a blood moon take place.
	 // Set to "0" for no blood moons.
	BloodMoonFrequency int
	 // How many days can the actual blood moon day randomly deviate from the above setting.
	 // Setting this to 0 makes blood moons happen exactly each Nth day as specified in BloodMoonFrequency.
	BloodMoonRange int
	 // The Hour number that the red day number begins on a blood moon day.
	 // Setting this to -1 makes the red never show.
	BloodMoonWarning int
	 // This is the number of zombies that can be alive (spawned at the same time) at any time PER PLAYER during a blood moon horde, however, MaxSpawnedZombies overrides this number in multiplayer games.
	 // Also note that your game stage sets the max number of zombies PER PARTY.
	 // Low game stage values can result in lower number of zombies than the BloodMoonEnemyCount setting.
	 // Changing this setting has a huge impact on performance.
	BloodMoonEnemyCount int
	 // percentage in whole numbers.
	LootAbundance int
	 // days in whole numbers.
	LootRespawnDays int
	 // How often airdrop occur in game-hours, 0 == never.
	AirDropFrequency int
	 // Sets if a marker is added to map/compass for air drops.
	AirDropMarker bool
	 // The distance you must be within to receive party shared kill xp and quest party kill objective credit.
	PartySharedKillRange int
	 // Player Killing Settings (0 = No Killing, 1 = Kill Allies Only, 2 = Kill Strangers Only, 3 = Kill Everyone).
	PlayerKillingMode int
	 // Maximum allowed land claims per player.
	LandClaimCount int
	 // Size in blocks that is protected by a keystone.
	LandClaimSize int
	 // Keystones must be this many blocks apart (unless you are friends with the other player).
	LandClaimDeadZone int
	 // The number of real world days a player can be offline before their claims expire and are no longer protected.
	LandClaimExpiryTime int
	 // Controls how offline players land claims decay.
	 // 0=Slow (Linear) , 1=Fast (Exponential), 2=None (Full protection until claim is expired).
	LandClaimDecayMode int
	 // How much protected claim area block hardness is increased when a player is online.
	 // 0 means infinite (no damage will ever be taken).
	 // Default is 4x.
	LandClaimOnlineDurabilityModifier int
	 // How much protected claim area block hardness is increased when a player is offline.
	 // 0 means infinite (no damage will ever be taken).
	 // Default is 4x.
	LandClaimOfflineDurabilityModifier int
	 // The number of minutes after a player logs out that the land claim area hardness transitions from online to offline.
	 // Default is 0.
	LandClaimOfflineDelay int
	 // Is Dynamic Mesh system enabled.
	DynamicMeshEnabled bool
	 // Is Dynamic Mesh system only active in player LCB areas.
	DynamicMeshLandClaimOnly bool
	 // Dynamic Mesh LCB chunk radius.
	DynamicMeshLandClaimBuffer int
	 // How many items can be processed concurrently, higher values use more RAM.
	DynamicMeshMaxItemCache int
	 // Required permission level to use twitch integration on the server.
	TwitchServerPermission int
	 // If the server allows twitch actions during a blood moon.
	 // This could cause server lag with extra zombies being spawned during blood moon.
	TwitchBloodMoonAllowed bool
	 // Limits the number of quests that contribute to quest tier progression a player can complete each day.
	 // Quests after the limit can still be completed for rewards.
	QuestProgressionDailyLimit int
}


func CreateServerSettings(configFile string) (ss ServerSettings, err error) {
	var ssxml ServerSettingsBlock
	if configFile != "" {
		var bytes []byte
		if bytes,err = os.ReadFile(configFile); err != nil && errors.Is(err,os.ErrNotExist) {
			slog.Warn("mounted config doesn't exist",slog.Any("error",err))
			err = nil
		} else if err != nil {			
			return
		} else if err = xml.Unmarshal(bytes,&ssxml); err != nil {
			return			
		}		
	}
	// These settings are required to be set accordingly for container runtimes
	ss.UserDataFolder = "/home/steam/server"
	ss.ServerPort = 26900
	ss.WebDashboardPort = 8080
	ss.TelnetPort = 8081
	ss.AdminFileName = "serveradmin.xml"
			
	var got bool
	got = false
	if os.Getenv("SEVEN_ServerName") != "" {
		ss.ServerName = os.Getenv("SEVEN_ServerName")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerName"); ok {
			ss.ServerName = v
			got = true
		}
	} 
	if !got {
		ss.ServerName = "My Game Host"
	}
	got = false
	if os.Getenv("SEVEN_ServerDescription") != "" {
		ss.ServerDescription = os.Getenv("SEVEN_ServerDescription")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerDescription"); ok {
			ss.ServerDescription = v
			got = true
		}
	} 
	if !got {
		ss.ServerDescription = "A 7 Days to Die server"
	}
	got = false
	if os.Getenv("SEVEN_ServerWebsiteURL") != "" {
		ss.ServerWebsiteURL = os.Getenv("SEVEN_ServerWebsiteURL")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerWebsiteURL"); ok {
			ss.ServerWebsiteURL = v
			got = true
		}
	} 
	if !got {
		ss.ServerWebsiteURL = ""
	}
	got = false
	if os.Getenv("SEVEN_ServerPassword") != "" {
		ss.ServerPassword = os.Getenv("SEVEN_ServerPassword")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerPassword"); ok {
			ss.ServerPassword = v
			got = true
		}
	} 
	if !got {
		ss.ServerPassword = ""
	}
	got = false
	if os.Getenv("SEVEN_ServerLoginConfirmationText") != "" {
		ss.ServerLoginConfirmationText = os.Getenv("SEVEN_ServerLoginConfirmationText")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerLoginConfirmationText"); ok {
			ss.ServerLoginConfirmationText = v
			got = true
		}
	} 
	if !got {
		ss.ServerLoginConfirmationText = ""
	}
	got = false
	if os.Getenv("SEVEN_Region") != "" {
		ss.Region = os.Getenv("SEVEN_Region")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("Region"); ok {
			ss.Region = v
			got = true
		}
	} 
	if !got {
		ss.Region = "NorthAmericaEast"
	}
	got = false
	if os.Getenv("SEVEN_Language") != "" {
		ss.Language = os.Getenv("SEVEN_Language")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("Language"); ok {
			ss.Language = v
			got = true
		}
	} 
	if !got {
		ss.Language = "English"
	}
	got = false
	if os.Getenv("SEVEN_ServerVisibility") != "" {
		t := os.Getenv("SEVEN_ServerVisibility")
		ss.ServerVisibility,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerVisibility"); ok {
			ss.ServerVisibility,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ServerVisibility = 2
	}
	got = false
	if os.Getenv("SEVEN_ServerDisabledNetworkProtocols") != "" {
		ss.ServerDisabledNetworkProtocols = os.Getenv("SEVEN_ServerDisabledNetworkProtocols")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerDisabledNetworkProtocols"); ok {
			ss.ServerDisabledNetworkProtocols = v
			got = true
		}
	} 
	if !got {
		ss.ServerDisabledNetworkProtocols = "SteamNetworking"
	}
	got = false
	if os.Getenv("SEVEN_ServerMaxWorldTransferSpeedKiBs") != "" {
		t := os.Getenv("SEVEN_ServerMaxWorldTransferSpeedKiBs")
		ss.ServerMaxWorldTransferSpeedKiBs,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerMaxWorldTransferSpeedKiBs"); ok {
			ss.ServerMaxWorldTransferSpeedKiBs,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ServerMaxWorldTransferSpeedKiBs = 512
	}
	got = false
	if os.Getenv("SEVEN_ServerMaxPlayerCount") != "" {
		t := os.Getenv("SEVEN_ServerMaxPlayerCount")
		ss.ServerMaxPlayerCount,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerMaxPlayerCount"); ok {
			ss.ServerMaxPlayerCount,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ServerMaxPlayerCount = 8
	}
	got = false
	if os.Getenv("SEVEN_ServerReservedSlots") != "" {
		t := os.Getenv("SEVEN_ServerReservedSlots")
		ss.ServerReservedSlots,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerReservedSlots"); ok {
			ss.ServerReservedSlots,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ServerReservedSlots = 0
	}
	got = false
	if os.Getenv("SEVEN_ServerReservedSlotsPermission") != "" {
		t := os.Getenv("SEVEN_ServerReservedSlotsPermission")
		ss.ServerReservedSlotsPermission,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerReservedSlotsPermission"); ok {
			ss.ServerReservedSlotsPermission,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ServerReservedSlotsPermission = 100
	}
	got = false
	if os.Getenv("SEVEN_ServerAdminSlots") != "" {
		t := os.Getenv("SEVEN_ServerAdminSlots")
		ss.ServerAdminSlots,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerAdminSlots"); ok {
			ss.ServerAdminSlots,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ServerAdminSlots = 0
	}
	got = false
	if os.Getenv("SEVEN_ServerAdminSlotsPermission") != "" {
		t := os.Getenv("SEVEN_ServerAdminSlotsPermission")
		ss.ServerAdminSlotsPermission,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerAdminSlotsPermission"); ok {
			ss.ServerAdminSlotsPermission,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ServerAdminSlotsPermission = 0
	}
	got = false
	if os.Getenv("SEVEN_WebDashboardEnabled") != "" {
		t := os.Getenv("SEVEN_WebDashboardEnabled")
		ss.WebDashboardEnabled,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("WebDashboardEnabled"); ok {
			ss.WebDashboardEnabled,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.WebDashboardEnabled = false
	}
	got = false
	if os.Getenv("SEVEN_WebDashboardUrl") != "" {
		ss.WebDashboardUrl = os.Getenv("SEVEN_WebDashboardUrl")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("WebDashboardUrl"); ok {
			ss.WebDashboardUrl = v
			got = true
		}
	} 
	if !got {
		ss.WebDashboardUrl = ""
	}
	got = false
	if os.Getenv("SEVEN_EnableMapRendering") != "" {
		t := os.Getenv("SEVEN_EnableMapRendering")
		ss.EnableMapRendering,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("EnableMapRendering"); ok {
			ss.EnableMapRendering,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.EnableMapRendering = false
	}
	got = false
	if os.Getenv("SEVEN_TelnetEnabled") != "" {
		t := os.Getenv("SEVEN_TelnetEnabled")
		ss.TelnetEnabled,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("TelnetEnabled"); ok {
			ss.TelnetEnabled,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.TelnetEnabled = true
	}
	got = false
	if os.Getenv("SEVEN_TelnetPassword") != "" {
		ss.TelnetPassword = os.Getenv("SEVEN_TelnetPassword")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("TelnetPassword"); ok {
			ss.TelnetPassword = v
			got = true
		}
	} 
	if !got {
		ss.TelnetPassword = ""
	}
	got = false
	if os.Getenv("SEVEN_TelnetFailedLoginLimit") != "" {
		t := os.Getenv("SEVEN_TelnetFailedLoginLimit")
		ss.TelnetFailedLoginLimit,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("TelnetFailedLoginLimit"); ok {
			ss.TelnetFailedLoginLimit,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.TelnetFailedLoginLimit = 10
	}
	got = false
	if os.Getenv("SEVEN_TelnetFailedLoginsBlocktime") != "" {
		t := os.Getenv("SEVEN_TelnetFailedLoginsBlocktime")
		ss.TelnetFailedLoginsBlocktime,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("TelnetFailedLoginsBlocktime"); ok {
			ss.TelnetFailedLoginsBlocktime,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.TelnetFailedLoginsBlocktime = 10
	}
	got = false
	if os.Getenv("SEVEN_TerminalWindowEnabled") != "" {
		t := os.Getenv("SEVEN_TerminalWindowEnabled")
		ss.TerminalWindowEnabled,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("TerminalWindowEnabled"); ok {
			ss.TerminalWindowEnabled,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.TerminalWindowEnabled = true
	}
	got = false
	if os.Getenv("SEVEN_EACEnabled") != "" {
		t := os.Getenv("SEVEN_EACEnabled")
		ss.EACEnabled,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("EACEnabled"); ok {
			ss.EACEnabled,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.EACEnabled = true
	}
	got = false
	if os.Getenv("SEVEN_IgnoreEOSSanctions") != "" {
		t := os.Getenv("SEVEN_IgnoreEOSSanctions")
		ss.IgnoreEOSSanctions,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("IgnoreEOSSanctions"); ok {
			ss.IgnoreEOSSanctions,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.IgnoreEOSSanctions = false
	}
	got = false
	if os.Getenv("SEVEN_HideCommandExecutionLog") != "" {
		t := os.Getenv("SEVEN_HideCommandExecutionLog")
		ss.HideCommandExecutionLog,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("HideCommandExecutionLog"); ok {
			ss.HideCommandExecutionLog,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.HideCommandExecutionLog = 0
	}
	got = false
	if os.Getenv("SEVEN_MaxUncoveredMapChunksPerPlayer") != "" {
		t := os.Getenv("SEVEN_MaxUncoveredMapChunksPerPlayer")
		ss.MaxUncoveredMapChunksPerPlayer,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("MaxUncoveredMapChunksPerPlayer"); ok {
			ss.MaxUncoveredMapChunksPerPlayer,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.MaxUncoveredMapChunksPerPlayer = 131072
	}
	got = false
	if os.Getenv("SEVEN_PersistentPlayerProfiles") != "" {
		t := os.Getenv("SEVEN_PersistentPlayerProfiles")
		ss.PersistentPlayerProfiles,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("PersistentPlayerProfiles"); ok {
			ss.PersistentPlayerProfiles,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.PersistentPlayerProfiles = false
	}
	got = false
	if os.Getenv("SEVEN_MaxChunkAge") != "" {
		t := os.Getenv("SEVEN_MaxChunkAge")
		ss.MaxChunkAge,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("MaxChunkAge"); ok {
			ss.MaxChunkAge,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.MaxChunkAge = -1
	}
	got = false
	if os.Getenv("SEVEN_SaveDataLimit") != "" {
		t := os.Getenv("SEVEN_SaveDataLimit")
		ss.SaveDataLimit,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("SaveDataLimit"); ok {
			ss.SaveDataLimit,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.SaveDataLimit = -1
	}
	got = false
	if os.Getenv("SEVEN_GameWorld") != "" {
		ss.GameWorld = os.Getenv("SEVEN_GameWorld")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("GameWorld"); ok {
			ss.GameWorld = v
			got = true
		}
	} 
	if !got {
		ss.GameWorld = "Navezgane"
	}
	got = false
	if os.Getenv("SEVEN_WorldGenSeed") != "" {
		ss.WorldGenSeed = os.Getenv("SEVEN_WorldGenSeed")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("WorldGenSeed"); ok {
			ss.WorldGenSeed = v
			got = true
		}
	} 
	if !got {
		ss.WorldGenSeed = "asdf"
	}
	got = false
	if os.Getenv("SEVEN_WorldGenSize") != "" {
		t := os.Getenv("SEVEN_WorldGenSize")
		ss.WorldGenSize,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("WorldGenSize"); ok {
			ss.WorldGenSize,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.WorldGenSize = 6144
	}
	got = false
	if os.Getenv("SEVEN_GameName") != "" {
		ss.GameName = os.Getenv("SEVEN_GameName")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("GameName"); ok {
			ss.GameName = v
			got = true
		}
	} 
	if !got {
		ss.GameName = "My Game"
	}
	got = false
	if os.Getenv("SEVEN_GameMode") != "" {
		ss.GameMode = os.Getenv("SEVEN_GameMode")
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("GameMode"); ok {
			ss.GameMode = v
			got = true
		}
	} 
	if !got {
		ss.GameMode = "GameModeSurvival"
	}
	got = false
	if os.Getenv("SEVEN_GameDifficulty") != "" {
		t := os.Getenv("SEVEN_GameDifficulty")
		ss.GameDifficulty,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("GameDifficulty"); ok {
			ss.GameDifficulty,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.GameDifficulty = 1
	}
	got = false
	if os.Getenv("SEVEN_BlockDamagePlayer") != "" {
		t := os.Getenv("SEVEN_BlockDamagePlayer")
		ss.BlockDamagePlayer,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BlockDamagePlayer"); ok {
			ss.BlockDamagePlayer,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BlockDamagePlayer = 100
	}
	got = false
	if os.Getenv("SEVEN_BlockDamageAI") != "" {
		t := os.Getenv("SEVEN_BlockDamageAI")
		ss.BlockDamageAI,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BlockDamageAI"); ok {
			ss.BlockDamageAI,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BlockDamageAI = 100
	}
	got = false
	if os.Getenv("SEVEN_BlockDamageAIBM") != "" {
		t := os.Getenv("SEVEN_BlockDamageAIBM")
		ss.BlockDamageAIBM,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BlockDamageAIBM"); ok {
			ss.BlockDamageAIBM,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BlockDamageAIBM = 100
	}
	got = false
	if os.Getenv("SEVEN_XPMultiplier") != "" {
		t := os.Getenv("SEVEN_XPMultiplier")
		ss.XPMultiplier,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("XPMultiplier"); ok {
			ss.XPMultiplier,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.XPMultiplier = 100
	}
	got = false
	if os.Getenv("SEVEN_PlayerSafeZoneLevel") != "" {
		t := os.Getenv("SEVEN_PlayerSafeZoneLevel")
		ss.PlayerSafeZoneLevel,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("PlayerSafeZoneLevel"); ok {
			ss.PlayerSafeZoneLevel,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.PlayerSafeZoneLevel = 5
	}
	got = false
	if os.Getenv("SEVEN_PlayerSafeZoneHours") != "" {
		t := os.Getenv("SEVEN_PlayerSafeZoneHours")
		ss.PlayerSafeZoneHours,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("PlayerSafeZoneHours"); ok {
			ss.PlayerSafeZoneHours,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.PlayerSafeZoneHours = 5
	}
	got = false
	if os.Getenv("SEVEN_BuildCreate") != "" {
		t := os.Getenv("SEVEN_BuildCreate")
		ss.BuildCreate,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BuildCreate"); ok {
			ss.BuildCreate,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.BuildCreate = false
	}
	got = false
	if os.Getenv("SEVEN_DayNightLength") != "" {
		t := os.Getenv("SEVEN_DayNightLength")
		ss.DayNightLength,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DayNightLength"); ok {
			ss.DayNightLength,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.DayNightLength = 60
	}
	got = false
	if os.Getenv("SEVEN_DayLightLength") != "" {
		t := os.Getenv("SEVEN_DayLightLength")
		ss.DayLightLength,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DayLightLength"); ok {
			ss.DayLightLength,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.DayLightLength = 18
	}
	got = false
	if os.Getenv("SEVEN_DeathPenalty") != "" {
		t := os.Getenv("SEVEN_DeathPenalty")
		ss.DeathPenalty,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DeathPenalty"); ok {
			ss.DeathPenalty,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.DeathPenalty = 1
	}
	got = false
	if os.Getenv("SEVEN_DropOnDeath") != "" {
		t := os.Getenv("SEVEN_DropOnDeath")
		ss.DropOnDeath,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DropOnDeath"); ok {
			ss.DropOnDeath,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.DropOnDeath = 1
	}
	got = false
	if os.Getenv("SEVEN_DropOnQuit") != "" {
		t := os.Getenv("SEVEN_DropOnQuit")
		ss.DropOnQuit,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DropOnQuit"); ok {
			ss.DropOnQuit,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.DropOnQuit = 0
	}
	got = false
	if os.Getenv("SEVEN_BedrollDeadZoneSize") != "" {
		t := os.Getenv("SEVEN_BedrollDeadZoneSize")
		ss.BedrollDeadZoneSize,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BedrollDeadZoneSize"); ok {
			ss.BedrollDeadZoneSize,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BedrollDeadZoneSize = 15
	}
	got = false
	if os.Getenv("SEVEN_BedrollExpiryTime") != "" {
		t := os.Getenv("SEVEN_BedrollExpiryTime")
		ss.BedrollExpiryTime,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BedrollExpiryTime"); ok {
			ss.BedrollExpiryTime,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BedrollExpiryTime = 45
	}
	got = false
	if os.Getenv("SEVEN_MaxSpawnedZombies") != "" {
		t := os.Getenv("SEVEN_MaxSpawnedZombies")
		ss.MaxSpawnedZombies,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("MaxSpawnedZombies"); ok {
			ss.MaxSpawnedZombies,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.MaxSpawnedZombies = 64
	}
	got = false
	if os.Getenv("SEVEN_MaxSpawnedAnimals") != "" {
		t := os.Getenv("SEVEN_MaxSpawnedAnimals")
		ss.MaxSpawnedAnimals,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("MaxSpawnedAnimals"); ok {
			ss.MaxSpawnedAnimals,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.MaxSpawnedAnimals = 50
	}
	got = false
	if os.Getenv("SEVEN_ServerMaxAllowedViewDistance") != "" {
		t := os.Getenv("SEVEN_ServerMaxAllowedViewDistance")
		ss.ServerMaxAllowedViewDistance,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ServerMaxAllowedViewDistance"); ok {
			ss.ServerMaxAllowedViewDistance,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ServerMaxAllowedViewDistance = 12
	}
	got = false
	if os.Getenv("SEVEN_MaxQueuedMeshLayers") != "" {
		t := os.Getenv("SEVEN_MaxQueuedMeshLayers")
		ss.MaxQueuedMeshLayers,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("MaxQueuedMeshLayers"); ok {
			ss.MaxQueuedMeshLayers,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.MaxQueuedMeshLayers = 1000
	}
	got = false
	if os.Getenv("SEVEN_EnemySpawnMode") != "" {
		t := os.Getenv("SEVEN_EnemySpawnMode")
		ss.EnemySpawnMode,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("EnemySpawnMode"); ok {
			ss.EnemySpawnMode,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.EnemySpawnMode = true
	}
	got = false
	if os.Getenv("SEVEN_EnemyDifficulty") != "" {
		t := os.Getenv("SEVEN_EnemyDifficulty")
		ss.EnemyDifficulty,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("EnemyDifficulty"); ok {
			ss.EnemyDifficulty,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.EnemyDifficulty = 0
	}
	got = false
	if os.Getenv("SEVEN_ZombieFeralSense") != "" {
		t := os.Getenv("SEVEN_ZombieFeralSense")
		ss.ZombieFeralSense,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ZombieFeralSense"); ok {
			ss.ZombieFeralSense,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ZombieFeralSense = 0
	}
	got = false
	if os.Getenv("SEVEN_ZombieMove") != "" {
		t := os.Getenv("SEVEN_ZombieMove")
		ss.ZombieMove,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ZombieMove"); ok {
			ss.ZombieMove,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ZombieMove = 0
	}
	got = false
	if os.Getenv("SEVEN_ZombieMoveNight") != "" {
		t := os.Getenv("SEVEN_ZombieMoveNight")
		ss.ZombieMoveNight,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ZombieMoveNight"); ok {
			ss.ZombieMoveNight,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ZombieMoveNight = 3
	}
	got = false
	if os.Getenv("SEVEN_ZombieFeralMove") != "" {
		t := os.Getenv("SEVEN_ZombieFeralMove")
		ss.ZombieFeralMove,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ZombieFeralMove"); ok {
			ss.ZombieFeralMove,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ZombieFeralMove = 3
	}
	got = false
	if os.Getenv("SEVEN_ZombieBMMove") != "" {
		t := os.Getenv("SEVEN_ZombieBMMove")
		ss.ZombieBMMove,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("ZombieBMMove"); ok {
			ss.ZombieBMMove,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.ZombieBMMove = 3
	}
	got = false
	if os.Getenv("SEVEN_BloodMoonFrequency") != "" {
		t := os.Getenv("SEVEN_BloodMoonFrequency")
		ss.BloodMoonFrequency,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BloodMoonFrequency"); ok {
			ss.BloodMoonFrequency,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BloodMoonFrequency = 7
	}
	got = false
	if os.Getenv("SEVEN_BloodMoonRange") != "" {
		t := os.Getenv("SEVEN_BloodMoonRange")
		ss.BloodMoonRange,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BloodMoonRange"); ok {
			ss.BloodMoonRange,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BloodMoonRange = 0
	}
	got = false
	if os.Getenv("SEVEN_BloodMoonWarning") != "" {
		t := os.Getenv("SEVEN_BloodMoonWarning")
		ss.BloodMoonWarning,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BloodMoonWarning"); ok {
			ss.BloodMoonWarning,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BloodMoonWarning = 8
	}
	got = false
	if os.Getenv("SEVEN_BloodMoonEnemyCount") != "" {
		t := os.Getenv("SEVEN_BloodMoonEnemyCount")
		ss.BloodMoonEnemyCount,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("BloodMoonEnemyCount"); ok {
			ss.BloodMoonEnemyCount,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.BloodMoonEnemyCount = 8
	}
	got = false
	if os.Getenv("SEVEN_LootAbundance") != "" {
		t := os.Getenv("SEVEN_LootAbundance")
		ss.LootAbundance,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LootAbundance"); ok {
			ss.LootAbundance,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LootAbundance = 100
	}
	got = false
	if os.Getenv("SEVEN_LootRespawnDays") != "" {
		t := os.Getenv("SEVEN_LootRespawnDays")
		ss.LootRespawnDays,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LootRespawnDays"); ok {
			ss.LootRespawnDays,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LootRespawnDays = 7
	}
	got = false
	if os.Getenv("SEVEN_AirDropFrequency") != "" {
		t := os.Getenv("SEVEN_AirDropFrequency")
		ss.AirDropFrequency,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("AirDropFrequency"); ok {
			ss.AirDropFrequency,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.AirDropFrequency = 72
	}
	got = false
	if os.Getenv("SEVEN_AirDropMarker") != "" {
		t := os.Getenv("SEVEN_AirDropMarker")
		ss.AirDropMarker,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("AirDropMarker"); ok {
			ss.AirDropMarker,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.AirDropMarker = true
	}
	got = false
	if os.Getenv("SEVEN_PartySharedKillRange") != "" {
		t := os.Getenv("SEVEN_PartySharedKillRange")
		ss.PartySharedKillRange,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("PartySharedKillRange"); ok {
			ss.PartySharedKillRange,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.PartySharedKillRange = 100
	}
	got = false
	if os.Getenv("SEVEN_PlayerKillingMode") != "" {
		t := os.Getenv("SEVEN_PlayerKillingMode")
		ss.PlayerKillingMode,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("PlayerKillingMode"); ok {
			ss.PlayerKillingMode,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.PlayerKillingMode = 3
	}
	got = false
	if os.Getenv("SEVEN_LandClaimCount") != "" {
		t := os.Getenv("SEVEN_LandClaimCount")
		ss.LandClaimCount,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LandClaimCount"); ok {
			ss.LandClaimCount,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LandClaimCount = 3
	}
	got = false
	if os.Getenv("SEVEN_LandClaimSize") != "" {
		t := os.Getenv("SEVEN_LandClaimSize")
		ss.LandClaimSize,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LandClaimSize"); ok {
			ss.LandClaimSize,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LandClaimSize = 41
	}
	got = false
	if os.Getenv("SEVEN_LandClaimDeadZone") != "" {
		t := os.Getenv("SEVEN_LandClaimDeadZone")
		ss.LandClaimDeadZone,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LandClaimDeadZone"); ok {
			ss.LandClaimDeadZone,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LandClaimDeadZone = 30
	}
	got = false
	if os.Getenv("SEVEN_LandClaimExpiryTime") != "" {
		t := os.Getenv("SEVEN_LandClaimExpiryTime")
		ss.LandClaimExpiryTime,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LandClaimExpiryTime"); ok {
			ss.LandClaimExpiryTime,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LandClaimExpiryTime = 7
	}
	got = false
	if os.Getenv("SEVEN_LandClaimDecayMode") != "" {
		t := os.Getenv("SEVEN_LandClaimDecayMode")
		ss.LandClaimDecayMode,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LandClaimDecayMode"); ok {
			ss.LandClaimDecayMode,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LandClaimDecayMode = 0
	}
	got = false
	if os.Getenv("SEVEN_LandClaimOnlineDurabilityModifier") != "" {
		t := os.Getenv("SEVEN_LandClaimOnlineDurabilityModifier")
		ss.LandClaimOnlineDurabilityModifier,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LandClaimOnlineDurabilityModifier"); ok {
			ss.LandClaimOnlineDurabilityModifier,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LandClaimOnlineDurabilityModifier = 4
	}
	got = false
	if os.Getenv("SEVEN_LandClaimOfflineDurabilityModifier") != "" {
		t := os.Getenv("SEVEN_LandClaimOfflineDurabilityModifier")
		ss.LandClaimOfflineDurabilityModifier,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LandClaimOfflineDurabilityModifier"); ok {
			ss.LandClaimOfflineDurabilityModifier,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LandClaimOfflineDurabilityModifier = 4
	}
	got = false
	if os.Getenv("SEVEN_LandClaimOfflineDelay") != "" {
		t := os.Getenv("SEVEN_LandClaimOfflineDelay")
		ss.LandClaimOfflineDelay,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("LandClaimOfflineDelay"); ok {
			ss.LandClaimOfflineDelay,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.LandClaimOfflineDelay = 0
	}
	got = false
	if os.Getenv("SEVEN_DynamicMeshEnabled") != "" {
		t := os.Getenv("SEVEN_DynamicMeshEnabled")
		ss.DynamicMeshEnabled,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DynamicMeshEnabled"); ok {
			ss.DynamicMeshEnabled,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.DynamicMeshEnabled = true
	}
	got = false
	if os.Getenv("SEVEN_DynamicMeshLandClaimOnly") != "" {
		t := os.Getenv("SEVEN_DynamicMeshLandClaimOnly")
		ss.DynamicMeshLandClaimOnly,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DynamicMeshLandClaimOnly"); ok {
			ss.DynamicMeshLandClaimOnly,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.DynamicMeshLandClaimOnly = true
	}
	got = false
	if os.Getenv("SEVEN_DynamicMeshLandClaimBuffer") != "" {
		t := os.Getenv("SEVEN_DynamicMeshLandClaimBuffer")
		ss.DynamicMeshLandClaimBuffer,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DynamicMeshLandClaimBuffer"); ok {
			ss.DynamicMeshLandClaimBuffer,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.DynamicMeshLandClaimBuffer = 3
	}
	got = false
	if os.Getenv("SEVEN_DynamicMeshMaxItemCache") != "" {
		t := os.Getenv("SEVEN_DynamicMeshMaxItemCache")
		ss.DynamicMeshMaxItemCache,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("DynamicMeshMaxItemCache"); ok {
			ss.DynamicMeshMaxItemCache,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.DynamicMeshMaxItemCache = 3
	}
	got = false
	if os.Getenv("SEVEN_TwitchServerPermission") != "" {
		t := os.Getenv("SEVEN_TwitchServerPermission")
		ss.TwitchServerPermission,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("TwitchServerPermission"); ok {
			ss.TwitchServerPermission,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.TwitchServerPermission = 90
	}
	got = false
	if os.Getenv("SEVEN_TwitchBloodMoonAllowed") != "" {
		t := os.Getenv("SEVEN_TwitchBloodMoonAllowed")
		ss.TwitchBloodMoonAllowed,_ = strconv.ParseBool(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("TwitchBloodMoonAllowed"); ok {
			ss.TwitchBloodMoonAllowed,_ = strconv.ParseBool(v)
			got = true
		}
	} 
	if !got {
		ss.TwitchBloodMoonAllowed = false
	}
	got = false
	if os.Getenv("SEVEN_QuestProgressionDailyLimit") != "" {
		t := os.Getenv("SEVEN_QuestProgressionDailyLimit")
		ss.QuestProgressionDailyLimit,_ = strconv.Atoi(t)
		got = true
	} else if configFile != "" {
		if v,ok := ssxml.GetValue("QuestProgressionDailyLimit"); ok {
			ss.QuestProgressionDailyLimit,_ = strconv.Atoi(v)
			got = true
		}
	} 
	if !got {
		ss.QuestProgressionDailyLimit = 4
	}
	return ss, nil
}
		