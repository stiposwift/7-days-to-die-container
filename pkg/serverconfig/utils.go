package serverconfig

import (
	_ "embed"
	"log/slog"
	"os"
	"regexp"
	"slices"
	"strings"
	"text/template"
)

//go:embed serverconfig.xml
var serverconfig string

var gameNameRegex = regexp.MustCompile(`[A-Za-z0-9_. -]+`)

func WriteServerConfig(location string, c ServerSettings) (bool, error) {
	tpl, err := template.New("ServerSettings").Parse(serverconfig)
	if err != nil {
		slog.Error("temmplate parse failed")
		return false, err
	}
	var f *os.File
	f, err = os.Create(location)
	if err != nil {
		slog.Error("os.create failed")
		return false, err
	}
	err = tpl.Execute(f, c)
	if err == nil {
		return true, nil
	}
	slog.Error("template execute failed")
	return false, err
}

var ValidRegions = []string{
	"NorthAmericaEast", "NorthAmericaWest", "CentralAmerica", "SouthAmerica", "Europe", "Russia", "Asia", "MiddleEast", "Africa", "Oceania",
}
var ValidGameWorlds = []string{
	"RWG", "Navezgane", "Pregen04k1", "Pregen04k2", "Pregen04k3", "Pregen04k4", "Pregen06k1", "Pregen06k2", "Pregen06k3", "Pregen06k4", "Pregen08k1", "Pregen08k2", "Pregen08k3", "Pregen08k4", "Pregen10k1", "Pregen10k2", "Pregen10k3", "Pregen10k4",
}

func (s *ServerSettings) Validate() bool {
	valid := true
	slog.Info("Validating serverconfig")
	if !slices.Contains[[]string, string](ValidRegions, s.Region) {
		slog.Error("Region property invalid", slog.String("Region", s.Region), slog.String("Valid Regions", strings.Join(ValidRegions, ",")))
		valid = false
	}
	if s.ServerPort < 26900 || (s.ServerPort > 26905 && s.ServerPort < 27015) || s.ServerPort > 27020 {
		slog.Warn("ServerPort falls outside of LAN range (26900-26905,27015-27020), your server will not appear on the LAN page", slog.Int("ServerPort", s.ServerPort))
	}
	if s.ServerVisibility < 0 || s.ServerVisibility > 2 {
		slog.Error("Invalid server visibility", slog.Int("ServerVisibility", s.ServerVisibility))
		slog.Error("0 = Unlisted")
		slog.Error("1 = Friends only")
		slog.Error("2 = Public")
		valid = false
	}
	if !strings.Contains(s.ServerDisabledNetworkProtocols, "SteamNetworking") {
		slog.Error("SteamNetworking should be disabled for dedicated servers", slog.String("ServerDisabledNetworkProtocols", s.ServerDisabledNetworkProtocols))
		valid = false
	}
	if s.ServerMaxWorldTransferSpeedKiBs > 1300 {
		slog.Warn("Setting a ServerMaxWorldTransferSpeedKiBs higher than 1300 has no effect", slog.Int("ServerMaxWorldTransferSpeedKiBs", s.ServerMaxWorldTransferSpeedKiBs))
	}
	if s.TelnetEnabled && s.TelnetPassword == "" {
		slog.Warn("Telnet is enabled but no password is set. It will only listen on local loopback")
	}
	if s.HideCommandExecutionLog < 0 || s.HideCommandExecutionLog > 3 {
		slog.Error("Invalid value for HideCommandExecutionLog, should be one of 0,1,2,3", slog.Int("HideCommandExecutionLog", s.HideCommandExecutionLog))
		slog.Error("0 = Show all")
		slog.Error("1 = Hide from Telnet/ControlPanel")
		slog.Error("2 = 1 and hide from Remote game clients")
		slog.Error("3 = Hide all")
		valid = false
	}

	if !slices.Contains[[]string, string](ValidGameWorlds, s.GameWorld) {
		slog.Error("Invalid GameWorld", slog.String("GameWorld", s.GameWorld))
	}

	if s.WorldGenSize < 6144 {
		slog.Error("World is too small (cannot be smaller than 6144)", slog.Int("WorldGenSize", s.WorldGenSize))
		valid = false
	} else if s.WorldGenSize > 10240 {
		slog.Error("World is too large (cannot be greater than 10240)", slog.Int("WorldGenSize", s.WorldGenSize))
		valid = false
	} else if s.WorldGenSize%2048 != 0 {
		slog.Error("World size must be evenly divisible by 2048!", slog.Int("WorldGenSize", s.WorldGenSize))
		valid = false
	}

	if !gameNameRegex.MatchString(s.GameName) {
		slog.Error("GameName is invalid, must match the regex `[A-Za-z0-9_-. ]+`", slog.String("GameName", s.GameName))
		valid = false
	}

	if s.GameDifficulty < 0 || s.GameDifficulty > 5 {
		slog.Error("GameDifficulty should be in the range of 0 to 5 (easy to hard)", slog.Int("GameDifficulty", s.GameDifficulty))
		valid = false
	}

	if s.DeathPenalty < 0 || s.DeathPenalty > 3 {
		slog.Error("DeathPenalty is invalid, should be one of 0,1,2,3", slog.Int("DeathPenalty", s.DeathPenalty))
		slog.Error("0 = Nothing")
		slog.Error("1 = Classic / XP Penalty")
		slog.Error("2 = Injured (Keep Debuffs, food/water at 50%)")
		slog.Error("3 = Permanent (Character deleted)")
		valid = false
	}

	if s.DropOnDeath < 0 || s.DropOnDeath > 4 {
		slog.Error("DropOnDeath is invalid, should be one of 0,1,2,3,4", slog.Int("DropOnDeath", s.DropOnDeath))
		slog.Error("0 = Nothing")
		slog.Error("1 = Everything")
		slog.Error("2 = Toolbelt")
		slog.Error("3 = Backpack")
		slog.Error("4 = Destroy All items")
		valid = false
	}
	if s.DropOnQuit < 0 || s.DropOnQuit > 3 {
		slog.Error("DropOnQuit is invalid, should be one of 0,1,2,3", slog.Int("DropOnQuit", s.DropOnQuit))
		valid = false
	}

	if s.ServerMaxAllowedViewDistance < 6 || s.ServerMaxAllowedViewDistance > 12 {
		slog.Error("ServerMaxAllowedViewDistance is invalid, should be between 6 and 12", slog.Int("ServerMaxAllowedViewDistance", s.ServerMaxAllowedViewDistance))
		valid = false
	}

	if s.EnemyDifficulty < 0 || s.EnemyDifficulty > 1 {
		slog.Error("EnemyDifficulty is invalid, should be 0 or 1", slog.Int("EnemyDifficulty", s.EnemyDifficulty))
		slog.Error("0 = Normal")
		slog.Error("1 = Feral")
		valid = false
	}

	if s.ZombieFeralSense < 0 || s.ZombieFeralSense > 3 {
		slog.Error("ZombieFeralSense is invalid, should be one of 0,1,2,3", slog.Int("ZombieFeralSense", s.ZombieFeralSense))
		slog.Error("0 = Off")
		slog.Error("1 = Day")
		slog.Error("2 = Night")
		slog.Error("3 = Always")
		valid = false
	}

	if s.ZombieMove < 0 || s.ZombieMove > 4 {
		slog.Error("ZombieMove is invalid, should be one of 0,1,2,3,4", slog.Int("ZombieMove", s.ZombieMove))
		slog.Error("0 = Walk")
		slog.Error("1 = Jog")
		slog.Error("2 = Run")
		slog.Error("3 = Sprint")
		slog.Error("4 = Nightmare")
		valid = false
	}

	if s.ZombieMoveNight < 0 || s.ZombieMoveNight > 4 {
		slog.Error("ZombieMoveNight is invalid, should be one of 0,1,2,3,4", slog.Int("ZombieMoveNight", s.ZombieMoveNight))
		slog.Error("0 = Walk")
		slog.Error("1 = Jog")
		slog.Error("2 = Run")
		slog.Error("3 = Sprint")
		slog.Error("4 = Nightmare")
		valid = false
	}

	if s.ZombieFeralMove < 0 || s.ZombieFeralMove > 4 {
		slog.Error("ZombieFeralMove is invalid, should be one of 0,1,2,3,4", slog.Int("ZombieFeralMove", s.ZombieFeralMove))
		slog.Error("0 = Walk")
		slog.Error("1 = Jog")
		slog.Error("2 = Run")
		slog.Error("3 = Sprint")
		slog.Error("4 = Nightmare")
		valid = false
	}

	if s.ZombieBMMove < 0 || s.ZombieBMMove > 4 {
		slog.Error("ZombieBMMove is invalid, should be one of 0,1,2,3,4", slog.Int("ZombieBMMove", s.ZombieBMMove))
		slog.Error("0 = Walk")
		slog.Error("1 = Jog")
		slog.Error("2 = Run")
		slog.Error("3 = Sprint")
		slog.Error("4 = Nightmare")
		valid = false
	}

	if s.PlayerKillingMode < 0 || s.PlayerKillingMode > 3 {
		slog.Error("PlayerKillingMode is invalid, should be one of 0,1,2,3", slog.Int("PlayerKillingMode", s.PlayerKillingMode))
		slog.Error("0 = No Killing")
		slog.Error("1 = Kill Allies Only")
		slog.Error("2 = Kill Strangers Only")
		slog.Error("3 = Kill Everyone")
		valid = false

	}

	return valid
}
