package install

import "gitlab.com/dmckernan/7-days-to-die-container/pkg/steam"

func Install7Days(installDir, betaVersion string) error {
	return steam.InstallGame(294420, installDir, betaVersion)
}
