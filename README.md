# 7 Days to Die - Container

## Building

You can build this docker image with the command:

```shell
docker build -t seven:latest .
```

## Setup

When running this container it is recommended to bind a local volume to the container to store your game install as well as the server files to, otherwise it will die with the container.

First create a local directory to install the game to:

```shell
mkdir -p ~/seven-days-container/game
```

Next create a local directory to store server savegame files in:

```shell
mkdir -p ~/seven-days-container/server
```

### Serverconfig

You should also create a `serverconfig.xml` to supply to your server, this can be found in many places but also comes with your 7 Days to Die installation, check the game folder.

> NOTE:&nbsp;
> You can also rely on JIT (just-in-time) generation of the `serverconfig.xml` with this image, simply set an env var with the prefix `SEVEN_` of the same-named value in the `serverconfig.xml` to set it. (For example `SEVEN_ServerName=My cool server`)

### Serveradmin

You can supply a `serveradmin.xml` to the run if desired, this can be found online or it will be generates with your 7 Days to Die server.

## Running in Docker

The container expects things to be mounted in specific spots in order to work correctly.
It will copy these mount locations to the spots that they belong inside of the container.

This table describes what to mount where

| Mount Item | Description | Mount Path |
| ---------- | ----------- | ---------- |
| Game       | This is the directory that contains the game files, not your server's save file, it gets mounted in for reusability and to keep the image size down | /home/steam/games/7dtd |
| Server Save | This is the directory that contains the server save files, all map and player data, mounted in so that it doesn't die with your container | /home/steam/server |
| Mods | This directory should contain any mods you want to install on the server, this needs to be supplied on every run. | /home/steam/mods/7dtd |
| serverconfig.xml | This is your serverconfig.xml file | /home/steam/serverconfig.xml |
| serveradmin.xml | This is your serveradmin.xml file | /home/steam/serveradmin.xml |
| server.log | This is the server log file, you will also see the output of this from the container, but it gets mounted in for saving in case of crashes | /home/steam/server.log |

A full docker run command might look like this:

> NOTE:&nbsp;
> You must always bind to port `26900` within the container, you can bind it to whatever port you want on the host machine. Even if you change the port in your `serverconfig.xml`, it will be reverted.
> You must also bind your TCP port +2 as UDP. So if you bound container 26900 to host 26900, you need to bind container 26902/udp to host 26902/udp, but if you bound container 26900 to host 27500, you need to bind container 26902/udp to host 27502/udp

```shell
docker run --rm -it \
-v ~/seven-days-container/game:/home/steam/games/7dtd \
-v ~/seven-days-container/server:/home/steam/server \
-v ~/seven-days-container/serverconfig.xml:/home/steam/serverconfig.xml \
-v ~/seven-days-container/serveradmin.xml:/home/steam/serveradmin.xml \
-v ~/seven-days-container/server.log:/home/steam/server.log \
-p 26900:26900 \
-p 26902:26902/udp \
seven:latest --beta latest_experimental
```

> IMPORTANT:&nbsp;
> If you are using the Telnet or Webdashboard features you must bind those ports as well. They are always their default values, like the game port. (Webdashboard = 8080, Telnet = 8081)

> NOTE:&nbsp;
> Omit the beta flag to install the stable release

This command will install the 7 Days to Die game server the very first time it is run, afterward the mounted directory should contain the files needed.
It will be updated if there is an update to the game files avaialable the next time it is run for the selected beta.

If this is the first time running the server, world generation could take a long time.
