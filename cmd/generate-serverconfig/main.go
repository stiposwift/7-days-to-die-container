package main

import (
	_ "embed"
	"fmt"
	"log/slog"
	"os"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

// This go script takes in a serverconfig.xml and outputs a go type struct for it, with comments.
// It is a utility to help make updating this tool easier

//go:embed serverconfig.xml
var serverconfig string

var structBuilder = strings.Builder{}
var templateBuilder = strings.Builder{}
var constructorBuilder = strings.Builder{}

var hardCodedSettings = []string{
	"AdminFileName",
	"ServerPort",
	"TelnetPort",
	"UserDataFolder",
	"WebDashboardPort",
}

func main() {

	gofileLocation := os.Getenv("GOFILE_LOCATION")
	templateLocation := os.Getenv("TEMPLATE_LOCATION")

	// Pre-fill the template
	templateBuilder.WriteString(`<?xml version="1.0"?>
<ServerSettings>
`)
	// Pre-fill the struct
	structBuilder.WriteString(`type ServerSettings struct {
`)
	// Pre-fill the create
	constructorBuilder.WriteString(`func CreateServerSettings(configFile string) (ss ServerSettings, err error) {
	var ssxml ServerSettingsBlock
	if configFile != "" {
		var bytes []byte
		if bytes,err = os.ReadFile(configFile); err != nil && errors.Is(err,os.ErrNotExist) {
			slog.Warn("mounted config doesn't exist",slog.Any("error",err))
			err = nil
		} else if err != nil {			
			return
		} else if err = xml.Unmarshal(bytes,&ssxml); err != nil {
			return			
		}		
	}
	// These settings are required to be set accordingly for container runtimes
	ss.ServerPort = 26900
	ss.WebDashboardPort = 8080
	ss.TelnetPort = 8081
	ss.AdminFileName = "serveradmin.xml"
	ss.UserDataFolder = "/home/steam/server"
			
	var got bool
`)

	pRex := regexp.MustCompile(`\s*(<!--)?<property\s+name="([^"]*)"\s*value="([^"]*)"\s*/>(\s*-->)?\s*<!-- (.*) -->`)
	properties := pRex.FindAllStringSubmatch(serverconfig, -1)
	var longest int
	for _, items := range properties {
		if len(items[2]) > longest {
			longest = len(items[2])
		}
	}
	for _, items := range properties {
		cName := items[2]
		cValue := items[3]
		cType := determineType(cValue)
		docs := items[5]
		printDocumentation(&structBuilder, 16, docs)
		structBuilder.WriteString(fmt.Sprintf("\t%s %s\n", cName, cType))
		templateBuilder.WriteString(
			fmt.Sprintf(
				"\t<property name=\"%s\"%*svalue=\"%s\" />%*s<!-- %s -->\n",
				cName,
				(longest - len(cName)), " ",
				fmt.Sprintf("{{ .%s }}", cName),
				((longest + 7) - len(cName)), " ",
				docs,
			),
		)

		// Handle special settings by ignoring them
		if slices.Contains[[]string, string](hardCodedSettings, cName) {
			continue
		}

		constructorBuilder.WriteString(fmt.Sprintf("\tgot = false\n\tif os.Getenv(\"SEVEN_%s\") != \"\" {\n", cName))
		if cType == "int" {
			constructorBuilder.WriteString(fmt.Sprintf("\t\tt := os.Getenv(\"SEVEN_%s\")\n", cName))
			constructorBuilder.WriteString(fmt.Sprintf("\t\tss.%s,_ = strconv.Atoi(t)\n", cName))
			constructorBuilder.WriteString("\t\tgot = true\n")
		} else if cType == "int64" {
			constructorBuilder.WriteString(fmt.Sprintf("\t\tt := os.Getenv(\"SEVEN_%s\")\n", cName))
			constructorBuilder.WriteString(fmt.Sprintf("\t\tss.%s,_ = strconv.ParseInt(t,10,64)\n", cName))
			constructorBuilder.WriteString("\t\tgot = true\n")
		} else if cType == "bool" {
			constructorBuilder.WriteString(fmt.Sprintf("\t\tt := os.Getenv(\"SEVEN_%s\")\n", cName))
			constructorBuilder.WriteString(fmt.Sprintf("\t\tss.%s,_ = strconv.ParseBool(t)\n", cName))
			constructorBuilder.WriteString("\t\tgot = true\n")
		} else {
			constructorBuilder.WriteString(fmt.Sprintf("\t\tss.%s = os.Getenv(\"SEVEN_%s\")\n", cName, cName))
			constructorBuilder.WriteString("\t\tgot = true\n")
		}
		goCValue := cValue
		if cType == "string" {
			goCValue = fmt.Sprintf("\"%s\"", cValue)
		}
		constructorBuilder.WriteString("\t} else if configFile != \"\" {\n")
		if cType == "int" {
			constructorBuilder.WriteString(fmt.Sprintf("\t\tif v,ok := ssxml.GetValue(\"%s\"); ok {\n", cName))
			constructorBuilder.WriteString(fmt.Sprintf("\t\t\tss.%s,_ = strconv.Atoi(v)\n", cName))
			constructorBuilder.WriteString("\t\t\tgot = true\n\t\t}\n")
		} else if cType == "int64" {
			constructorBuilder.WriteString(fmt.Sprintf("\t\tif v,ok := ssxml.GetValue(\"%s\"); ok {\n", cName))
			constructorBuilder.WriteString(fmt.Sprintf("\t\t\tss.%s,_ = strconv.ParseInt(v,10,64)\n", cName))
			constructorBuilder.WriteString("\t\t\tgot = true\n\t\t}\n")
		} else if cType == "bool" {
			constructorBuilder.WriteString(fmt.Sprintf("\t\tif v,ok := ssxml.GetValue(\"%s\"); ok {\n", cName))
			constructorBuilder.WriteString(fmt.Sprintf("\t\t\tss.%s,_ = strconv.ParseBool(v)\n", cName))
			constructorBuilder.WriteString("\t\t\tgot = true\n\t\t}\n")
		} else {
			constructorBuilder.WriteString(fmt.Sprintf("\t\tif v,ok := ssxml.GetValue(\"%s\"); ok {\n", cName))
			constructorBuilder.WriteString(fmt.Sprintf("\t\t\tss.%s = v\n", cName))
			constructorBuilder.WriteString("\t\t\tgot = true\n\t\t}\n")
		}

		constructorBuilder.WriteString(
			fmt.Sprintf(`	} 
	if !got {
		ss.%s = %s
	}
`, cName, goCValue),
		)
	}

	// Finish struct
	structBuilder.WriteString("}\n")
	// Finish template
	templateBuilder.WriteString("</ServerSettings>\n")
	constructorBuilder.WriteString("\treturn ss, nil\n}\n")

	if gofileLocation != "" {
		err := os.WriteFile(
			gofileLocation,
			[]byte(
				fmt.Sprintf(
					`// This code is generated
// Do not edit it directly
// Rerun the generate-serverconfig main package
//
// GOFILE_LOCATION=%s go run cmd/generate-serverconfig/main.go

package serverconfig

import "strconv"
import "os"
import "encoding/xml"
import "errors"

%s

%s		`, gofileLocation,
					structBuilder.String(),
					constructorBuilder.String(),
				),
			),
			0777,
		)
		if err != nil {
			slog.Error("failed to write serverconfig go file", slog.String("gofileLocation", gofileLocation), slog.Any("error", err))
		} else {
			slog.Info("wrote gofile", slog.String("gofileLocation", gofileLocation))
		}
	} else {
		fmt.Println("STRUCT OUTPUT\n", structBuilder.String())
		fmt.Println("CONSTRUCTOR OUTPUT\n", constructorBuilder.String())
	}

	if templateLocation != "" {
		err := os.WriteFile(templateLocation, []byte(templateBuilder.String()), 0777)
		if err != nil {
			slog.Error("failed to write serverconfig template file", slog.String("templateLocation", templateLocation), slog.Any("error", err))
		} else {
			slog.Info("wrote template", slog.String("templateLocation", templateLocation))
		}
	} else {
		fmt.Println("TEMPLATE OUTPUT\n", templateBuilder.String())
	}
}

func determineType(value string) (t string) {
	t = "string"
	if _, err := strconv.Atoi(value); err == nil {
		t = "int"
	} else if _, err := strconv.ParseInt(value, 10, 64); err == nil {
		t = "int64"
	} else if _, err := strconv.ParseBool(value); err == nil {
		t = "bool"
	}
	return t
}

func printDocumentation(b *strings.Builder, wordsPerLine int, doc string) {
	// Replace three periods with ellipses
	doc = strings.ReplaceAll(doc, "...", "…")
	// Split on periods
	sentences := strings.Split(doc, ".")
	for _, s := range sentences {
		if strings.Trim(s, " ") == "" {
			continue
		}
		// Split on words
		words := strings.Split(s, " ")
		ct := 0
		b.WriteString("\t //")
		for _, w := range words {
			if strings.Trim(w, " ") == "" {
				continue
			}
			if ct >= wordsPerLine {
				b.WriteString("\t //")
				ct = 0
			}
			b.WriteString(" " + w)
		}
		b.WriteString(".\n")
	}
}
