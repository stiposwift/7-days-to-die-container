package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log/slog"
	"os"
	"os/exec"

	"gitlab.com/dmckernan/7-days-to-die-container/pkg/install"
	"gitlab.com/dmckernan/7-days-to-die-container/pkg/server"
	"gitlab.com/dmckernan/7-days-to-die-container/pkg/serverconfig"
)

const (
	ExitCodeOk = iota
	ExitCodeErrorCreatingServerSettings
	ExitCodeErrorWritingServerConfig
	ExitCodeErrorInstall7DaysToDie
	ExitCodeErrorCreatingGameDir
	ExitCodeErrorCopyingServerFiles
	ExitCodeErrorRunning7DaysToDie
)

const (
	gameDir            = "/home/steam/7dtd-server-files"
	adminFileLocation  = "/home/steam/server/Saves/serveradmin.xml"
	configFileLocation = "/home/steam/server/serverconfig.xml"

	mountedGameDir = "/home/steam/games/7dtd"
	mountedModsDir = "/home/steam/mods/7dtd"

	mountedAdminFile  = "/home/steam/serveradmin.xml"
	mountedConfigFile = "/home/steam/serverconfig.xml"
	mountedLogFile    = "/home/steam/server.log"
)

func main() {
	var betaVersion string
	flag.StringVar(&betaVersion, "beta", "", "Beta candidate to install")

	// Write config if not exists
	serverSettings, err := serverconfig.CreateServerSettings(mountedConfigFile)
	if err != nil {
		slog.Error("error creating server settings", slog.Any("error", err))
		os.Exit(ExitCodeErrorCreatingServerSettings)
	}
	if ok, err := serverconfig.WriteServerConfig(configFileLocation, serverSettings); err != nil {
		slog.Error("error writing server config", slog.Any("error", err))
		os.Exit(ExitCodeErrorWritingServerConfig)
	} else if !ok {
		slog.Error("unknown error writing server config")
		os.Exit(ExitCodeErrorWritingServerConfig)
	}

	// Run the install, will update the game if its mounted in.
	if err := install.Install7Days(mountedGameDir, betaVersion); err != nil {
		slog.Error("error installing 7 Days to Die", slog.Any("error", err))
		os.Exit(ExitCodeErrorInstall7DaysToDie)
	}

	modsDir := fmt.Sprintf("%s/Mods", gameDir)

	// Delete the gamedir if it already exists
	_ = os.RemoveAll(gameDir)

	// Copy files from installDir to gameDir
	if err = os.CopyFS(gameDir, os.DirFS(mountedGameDir)); err != nil {
		slog.Error("error copying mounted game to local fs", slog.Any("error", err))
		os.Exit(ExitCodeErrorCopyingServerFiles)
	}

	// Copy files from the modsDir to gameDir
	if _, err = os.Stat(mountedModsDir); err != nil && errors.Is(err, os.ErrNotExist) {
		slog.Info("no mods found")
	} else if err != nil {
		slog.Warn("error copying mods into container", slog.Any("error", err))
	} else if err = os.CopyFS(modsDir, os.DirFS(mountedModsDir)); err != nil {
		slog.Warn("error copying mods into container, skipping mods integration", slog.Any("error", err))
	}

	// Copy serveradmin if present
	if _, err := os.Stat(mountedAdminFile); err != nil && errors.Is(err, os.ErrNotExist) {
		slog.Info("no serveradmin.xml found")
	} else if err != nil {
		slog.Warn("error copying mounted serveradmin.xml, skipping", slog.Any("error", err))
	} else {
		var f *os.File
		if f, err = os.OpenFile(adminFileLocation, os.O_TRUNC, 0777); err != nil {
			slog.Warn("error opening destination adminfile for writing", slog.Any("error", err))
		} else {
			var f2 *os.File
			if f2, err = os.Open(mountedAdminFile); err != nil {
				slog.Warn("error opening source adminfile for reading", slog.Any("error", err))
			} else if _, err = io.Copy(f, f2); err != nil {
				slog.Warn("error copying mounted serveradmin.xml, skipping", slog.Any("error", err))
			}
		}
	}

	// Start the server
	if err := server.Run7Days(gameDir, configFileLocation, mountedLogFile); err != nil {
		slog.Error("error running 7 Days to Die", slog.Any("error", err))
		slog.Error("dumping serverconfig\n")
		cmd := exec.Command("cat", configFileLocation)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Run()
		os.Exit(ExitCodeErrorRunning7DaysToDie)
	}
	os.Exit(ExitCodeOk)
}
